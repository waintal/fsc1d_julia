\documentclass[prb,11pt,twocolumn,superscriptaddress,tightenlines]{revtex4-1}

\usepackage{color}
\usepackage{ amssymb }

\begin{document}

\title{A small1D Quantum-Poisson solver}
\author{Xavier Waintal}
\affiliation{Univ. Grenoble Alpes, CEA, INAC-Pheliqs, 38000 Grenoble, France}
\date{August 2019}


\maketitle

\section{Poisson equation}

\subsection{Continuum}
S.I. form:
\begin{equation}
 \partial_z [ \epsilon_r (z) \partial_z U(z) ] = \frac{e}{\epsilon_0}  [n_d(z) -  n(z) ]
\end{equation}

adimensional form:
\begin{equation}
 \partial_{\bar z} [ \epsilon_r (\bar z) \partial_{\bar z} U(\bar z) ] = [\bar n_d (\bar z) - \bar n(\bar z) ]
\end{equation}
with $z= a\bar z$ and $\bar n =   \frac{ea^2}{\epsilon_0} n$ The scaling factor $a= 10^{-9}$ translates meters to nm.
$\frac{ea^2}{\epsilon_0} = 1.8095.\ 10^{-26}$.

\subsection{Discrete}

We devide the systems into $N$ cells $i$ where we suppose that the (relative) dielectric constant is
constant with value $\epsilon_i$ and the position of the center of the cell is $z_i$. We denote $q_i$ the total charge inside
the cell,
\begin{equation}
  q_i = \int_{\bar z \in i} \bar n(\bar z)d\bar z
\end{equation}
and $U_i = U(\bar z = z_i)$. The electric field $E_i$ is defined at the border between cell $i$ and cell $i-1$.
Gauss law takes the (exact) form:
\begin{equation}
q_i = \epsilon_i[ E_{i+1}-E_i]
\end{equation}
while $E=\partial U)$ is approximated by, 
\begin{equation}
E_i = \frac{U_i -U_{i-1}}{z_i-z_{i-1}}
\end{equation}
Introducing
\begin{eqnarray}
  c_{i} = \frac{\epsilon_i}{z_i-z_{i-1}}\\
  d_{i} = \frac{\epsilon_{i}}{z_{i+1}-z_{i}}
\end{eqnarray}
One gets
\begin{equation}
q_i = c_{i} V_{i-1} + d_{i} V_{i+1} -\left[c_{i} + d_{i} \right] V_{i}
\end{equation}


Boundary condition in the case of Neumann take the form:
\begin{eqnarray}
  q_1 =  d_{1} V_{2} - d_{1} V_{1}\\
  q_N = c_N V_{N-1} -c_N V_N
\end{eqnarray}
In the case of Dirichlet, we exchange the role of $q_1$ and $V_1$ ($q_N$ and $V_N$). The later become an input
(the value of the gate) and we solve for the former. We arrive at,
\begin{eqnarray}
  V_1 &=&   V_{2} - \frac{1}{d_{1}} q_{1}\\
  q_2 &=& -\frac{c_2}{d_1}  q_{1} + d_2 V_2 -d_2 V_2
\end{eqnarray}
and
\begin{eqnarray}
  V_{N} &=&   V_{N-1} - \frac{1}{c_{N}} q_{N}\\
  q_{N-1} &=& -\frac{c_N}{d_{N-1}}  q_{N} + c_{N-1} V_{N-2} -c_{N-1} V_{N-1}\nonumber
\end{eqnarray}

\section{Quantum equation}
\subsection{Continuum}
S.I. form in one dimension:
\begin{equation}
  -\left[\frac{\hbar^2}{2m(z)}\partial_{zz} +\partial_{zz}\frac{\hbar^2}{2m(z)}\right] \Psi (z) - eU(z) \Psi(z) = E \Psi
\end{equation}
The symmetric part is important to keep the Hamiltonian Hermitian (see Bastard PRB1981). When $m(z)$ abruptly
changes between two values $m_L$ and $m_R$, then $\Psi(z)$ remains continuus but its derivative is not. The relation between its left and right value is given by,
\begin{equation}
  \frac{1}{m_L}\partial_{z} \Psi_L =  \frac{1}{m_R}\partial_{z} \Psi_R
\end{equation}
\subsection{Discrete}
We discretize this equation using finite differences. We start from a mesh $z_i$ and suppose that the mass is constant and equal to $m_i$
in the corresponding cell. We note $\Psi_i=\Psi(z_i)$. The boundary between one cell and the next is at $b_i = (z_i + z_{i+1})/2$ (Different convention from Poisson which is not smart).
We need an estimate of $\partial_{zz} \psi_i$. Taylor expansion at $b_i$ provides:
\begin{eqnarray}
  \Psi_i &\approx& \Psi(b_i) - \partial_z \Psi (b_i,L) (z_{i+1}-z_i)/2   \\
  \Psi_{i+1} &\approx& \Psi(b_i) + \partial_z \Psi (b_i,R) (z_{i+1}-z_i)/2   \\
\end{eqnarray}
from which we get,
\begin{equation}
  \partial_z \Psi (b_i,L) \approx \frac{2m_i}{m_i+m_{i+1}}\frac{\Psi_{i+1}-\Psi_i}{z_{i+1}-z_i}
\end{equation}
Similarly,
\begin{equation}
  \partial_z \Psi (b_{i-1},R) \approx \frac{2m_i}{m_i+m_{i-1}}\frac{\Psi_{i}-\Psi_{i-1}}{z_{i}-z_{i-1}}
\end{equation}
Last, we estimate the curvature using a Taylor expansion of $\partial_z \Psi(z)$ around $z_i$,
\begin{equation}
 \partial_{zz} \psi_i = 2\frac{ \partial_z\Psi (b_i,L) -\partial_z \Psi (b_{i-1},R)}{z_{i+1}-z_{i-1}} 
\end{equation}
Putting things together, the discretized Hamiltonian $H_{ij}$ takes the form,
\begin{eqnarray}
  H_{i,i+1} &=&  - \frac{\hbar^2}{m_i+m_{i+1}}  \frac{2}{z_{i+1}-z_{i-1}}\frac{1}{z_{i+1}-z_{i}} \nonumber\\
  H_{i,i-1} &=&  - \frac{\hbar^2}{m_i+m_{i-1}}  \frac{2}{z_{i+1}-z_{i-1}}\frac{1}{z_{i}-z_{i-1}} \nonumber\\
  H_{i,i} &=&  - H_{i,i+1} - H_{i,i-1}  -eU_i
\end{eqnarray}
There is something fairly unfortunate about the above, which is that the corresponding Hamiltonian is not Hermitian!
Fortunately, Tan, Slider, Chang and Hu (1990, I don't have the ref yet) found a nice trick:
Introducing $L_i^2 = (z_{i+1}-z_{i-1})/2$, then we have
\begin{equation}
  H_{ij} = \frac{1}{L_i^2} \tilde H_{ij}
\end{equation}
where $\tilde H$ is Hermitian. defining
\begin{equation}
  \bar H_{ij} = \frac{1}{L_i} \tilde H_{ij}\frac{1}{L_j}
\end{equation}
then, $H\Psi = E\Psi$ becomes $\bar H \bar\Psi = E \bar\Psi$ with $\bar\Psi = L \Psi$ which is a nice regular Hermitian problem. From Tan et al,
apparently this converges very well. Remark: the above scaling factor $L_i$ is actually totally natural: the wave functions must be orthonormal,
\begin{equation}
  \int dz \Psi_1(z)^* \Psi_2 (z) = \delta_{1,2}
\end{equation}
Upon discretization of the above, factors $(z_{i+1}-z_i)$ appear that must be incorporated into the wave function
to restore orthonormality.
\begin{equation}
 1 = \int dz |\Psi(z)|^2  = \sum_i (z_{i+1}-z_i) |\Psi_i|^2\approx  \sum_i |\bar\Psi_i|^2 
\end{equation}
With these notations, $|\bar\Psi_i|^2$ is the total charge inside cell $i$ and is well normalized.

Units: we introduce the hopping unit $t=\hbar^2/(2m_ea^2)$ where $m_e$ is the bare electron mass and
we express $H$ and $E$ in these units.


\section{Self Consistent Problem}
\subsection{ILDOS}
\begin{equation}
 \int_y^\infty \frac{dE}{e^E +1} = \log (1+e^{-y})
\end{equation}
Not counting spins, the DOS of a 2D electron gas is $m^*/(2\pi\hbar^2)$ in $J^{-1}m^{-2}$. So
the total charge density is ($m^{-3}$),
\begin{equation}
  n(z,E_F) =k_BT \frac{m_e}{\pi\hbar^2} \times\nonumber
\end{equation}
\begin{equation}
  \sum_\alpha |\Psi_\alpha (z)|^2 m(z)\log \left[ 1+e^{(E_F-E_\alpha)/(k_BT)}\right]
\end{equation}
The total charge $n_i$ into a cell $i$ is given in $nm^{-2}$ by,
\begin{equation}
  n_i(E_F) = \frac{k_BT}{4\pi t} \times\nonumber
\end{equation}
\begin{equation}
  \sum_\alpha |\bar\Psi_\alpha (i)|^2 m_i\log \left[ 1+e^{(E_F-E_\alpha)/(k_BT)}\right]
\end{equation}
For Poisson equation, we have $q_i = (e/\epsilon_0) n_i$. Then the Poisson solver gives back Volts.
To go back to units of $t$, we need to do $U_i \rightarrow eU_i/t$ so putting things together,
we need to do $n_i \rightarrow \kappa n_i$ with
\begin{equation}
  \kappa = \frac{2 e^2 m_e a^2}{\epsilon_0\hbar^2} = 4.74 \ 10^{-7}
\end{equation}
Bottom line, if we use
\begin{equation}
  q_i = \kappa n_i
\end{equation}
in the Poisson equation, then all energies are in units of $t$.
\end{document}
