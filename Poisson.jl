""" 
    A simple module for solving 1D Poisson equation for a stack of semiconductors
"""
module Poisson
println("Loading module Poisson")

using SparseArrays

# All voltages U in Volts
# All charges q in number of electrons
# electron charge e > 0 so that for electrons q<0.

epsilon_0 = 8.8541878128e-12             # dielectric constant (Farad/meter)
ee = 1.60217662e-19                      # electron charge (Coulomb)
aa = 1e-9
conversion = ee/(aa*epsilon_0)           # conversion = 18.1 V.nm
                                         # *conversion transforms a density in [nm^-3] into 
                                         # 3D density in [V nm^-2]  
                                         # For the integrated charge per cell transforms [nm^-2] into
                                         # 2D density per cell in [V.nm^-1]

""" 
    Contains all the elements that describe a Poisson problem after its discretization 
    Potential is in Volt, distances in nm.
    Charges q_dopant and q are integrated per cell and in Volt/nm.
              - divide per conversion to get back to nm-2 i.e. a 2DEG par cell. 
              - then divide by position[i+1]-positions[i] to get a electronic density in nm^-3.
    See acompanied Notation.tex file.
"""
struct DiscretizedPoissonProblem
    n_cells :: Int                            # Total number of cells
    capa :: SparseMatrixCSC{Float64,Int}      # Capacitance matrix - essentially a discretized version of the Laplacian: C U = q 
    q_dopant :: Array{Float64,1}              # vector of dopant charges. Size: s. Units: Volt/nm
    epsilon :: Array{Float64,1}               # vector of discretize relative dielectric constant.
    positions :: Array{Float64,1}             # vector of positions of the center of each cell in nm. Size: s. Stack starts at z=0.
    gates :: Tuple{Bool,Bool}                  # (False, True) means no metallic gate at the bottom but one at the top.
end


"""
        Build the Discretized Poisson Problem from the mesh.
        
# Arguments
    - q_dopant :: Array{Float64,1}              # vector of dopant charges. Size: s. Units: Volt/nm^2
    - epsilon :: Array{Float64,1}               # vector of discretize relative dielectric constant.
    - positions :: Array{Float64,1}             # vector of positions of the center of each cell in nm. Size: s. Stack starts at z=0.
    - gates :: Tuple{Bool,Bool}                 # (False, True) means no metallic gate at the bottom but one at the top.
"""
function DiscretizedPoissonProblem(q_dopant :: Array{Float64,1},
                                   epsilon:: Array{Float64,1}  ,
                                   positions:: Array{Float64,1}  ,
                                   gates:: Tuple{Bool,Bool}  )
    n_cells = length(epsilon)
    if n_cells != length(positions)
        throw(InvalidStateException)
    end
    I =  zeros(Int,4*n_cells+10)
    J =  zeros(Int,4*n_cells+10)
    K =  zeros(Float64,4*n_cells+10)
    n_C_matrix = 0                      # Number of matrix elements in the Capacitance matrix.

    function update_C_matrix(i,j,val)
        n_C_matrix += 1
        I[n_C_matrix] = i
        J[n_C_matrix] =j
        K[n_C_matrix] = val
    end

    for i in 3:n_cells-1
        ci = epsilon[i]/(positions[i]-positions[i-1])
        update_C_matrix(i,i,-ci)
        update_C_matrix(i,i-1,+ci) 
    end
    for i in 2:n_cells-2
        di = epsilon[i]/(positions[i+1]-positions[i])
        update_C_matrix(i,i,-di)
        update_C_matrix(i,i+1,+di)
    end

    d1 = epsilon[1]/(positions[2]-positions[1])
    c2 = epsilon[2]/(positions[2]-positions[1])
    if gates[1]
        update_C_matrix(1,1,-1/d1)
        update_C_matrix(1,2,1.)
        update_C_matrix(2,1,-c2/d1) 
    else
        update_C_matrix(1,1,-d1)
        update_C_matrix(2,1,d1)  
        update_C_matrix(2,2,-c2)
        update_C_matrix(1,2,c2)  
    end
    
    dNM = epsilon[n_cells-1]/(positions[n_cells]-positions[n_cells-1])
    cN = epsilon[n_cells]/(positions[n_cells]-positions[n_cells-1])
    if gates[2]
        update_C_matrix(n_cells,n_cells,-1/cN)
        update_C_matrix(n_cells,n_cells-1,1.)  
        update_C_matrix(n_cells-1,n_cells,-cN/dNM)
    else
        update_C_matrix(n_cells,n_cells,-cN)
        update_C_matrix(n_cells,n_cells-1,cN)  
        update_C_matrix(n_cells-1,n_cells-1,-dNM)
        update_C_matrix(n_cells-1,n_cells,dNM)  
    end
    
    DiscretizedPoissonProblem(n_cells,
                              sparse(I[1:n_C_matrix],J[1:n_C_matrix],K[1:n_C_matrix]),
                              q_dopant,
                              epsilon,
                              positions,
                              gates)
end
                         
function solve_pb(poisson_pb ::DiscretizedPoissonProblem, U, q;V_bot = 0,V_top = 0)
    q .+= poisson_pb.q_dopant
    if poisson_pb.gates[1]
        q[1] = V_bot
    end
    if poisson_pb.gates[2]
        q[end] = V_top
    end   
    U .= poisson_pb.capa \q
end


end # module Poisson
