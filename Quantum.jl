module Quantum
println("Loading module Quantum")

const hbar = 1.0545718e-34
const m_e  = 9.10938356e-31
const aa = 1e-9
const t = hbar^2/(2*m_e*aa^2) 
const ee = 1.60217662e-19
const eV_2_t = ee/t
const rho = (aa^2*ee)*m_e/(pi*hbar^2)        # Bare DOS of the 2DEG (with spin) in aa^-2 / eV

using LinearAlgebra

""" 
    Contains all the elements that describe a Quantum problem after its discretization 
    Potential is in unit of t, distances in nm, mass in unit of the bare mass m_e.
    See acompanied Notation.tex file.
"""
struct DiscretizedQuantumProblem
    n_q :: Int                             # Total number of cells
    Diag :: Array{Float64,1}               # Diagonal part of the discretized Hamiltonian. Units: t
    Upper :: Array{Float64,1}              # Upper diagonal part of the discretized (tridiagonal) Hamiltonian. Units: t
    L :: Array{Float64,1}                  # non uniform discretization vector L_i = 1/(z_{i+1}-z_{i-1})^{1/2}
    mass :: Array{Float64,1}               # effective mass in unit of the bare mass
    positions :: Array{Float64,1}          # vector of positions of the center of each cell in nm. 
end


"""
        Build the Discretized Quantum Problem 

Essentially build the sparse Hamiltonian tridiagonal matrix H built from Diag and Upper.
  
# Arguments 
            positions   : in nm
            mass        : in bare mass
            U           : potential due to e.g. band offsets (an additional one can be provided upon solving). In unit of t.
"""
function DiscretizedQuantumProblem(positions :: Array{Float64,1},
                                   mass :: Array{Float64,1},
                                   U :: Array{Float64,1})
    n_q = length(positions)
    if n_q != length(mass)
        throw(InvalidStateException)
    end
    
    L = zeros(Float64,n_q)
    for i in 2:n_q-1
        L[i] = (positions[i+1]-positions[i-1])^0.5/2^(1/2)
    end
    L[1] = (positions[2]-positions[1])^0.5
    L[n_q] = (positions[n_q]-positions[n_q-1])^0.5
    
    Upper =  zeros(Float64,n_q-1)
    Diag =  zeros(Float64,n_q)
    

    for i in 1:n_q-1
        h_ij = 2/(mass[i]+mass[i+1])/(positions[i+1]-positions[i])
        Diag[i] += h_ij/L[i]/L[i]
        Diag[i+1] += h_ij/L[i+1]/L[i+1]
        Upper[i] -= h_ij/L[i]/L[i+1]
    end
    Diag[1]    /= 2
    Upper[1]   /= 2
    Diag[end]  /= 2
    Upper[end] /= 2
    
    Diag .+=  U 
        
 
    DiscretizedQuantumProblem(n_q,
                              Diag,
                              Upper,
                              L,
                              mass,
                              positions)
end

""" Calculate the N lowest eigentstates and eigen energies for a confining potnetial U """
function solve_pb(quantum_pb ::DiscretizedQuantumProblem, U,N)
    H_diag = quantum_pb.Diag .+ U
    H_tot = SymTridiagonal(H_diag,quantum_pb.Upper)
    Es = eigvals(H_tot)
    Psis = eigvecs(H_tot,Es[1:N])
    for i in 1:N
        Psis[:,i] .= Psis[:,i] ./quantum_pb.L
    end
    Es[1:N],Psis
end



end # module Quantum
