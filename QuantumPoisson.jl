include("./Quantum.jl")
include("./Poisson.jl")

module QuantumPoisson
println("Loading module QuantumPoisson")

using ..Poisson
using ..Quantum

struct BasicSelfConsistent1D
    positions :: Array{Float64,1} 
    start_quantum :: Int  # index where the quantum problem starts
    end_quantum :: Int    # index where the quantum problem ends
    P :: Poisson.DiscretizedPoissonProblem
    Q :: Quantum.DiscretizedQuantumProblem
    dst_quantum :: Array{Float64,1} # 2D dst of electron per quantum cell [nm^-2]
    U_poisson :: Array{Float64,1}   # Electric potential  in eV
    dx :: Array{Float64,1}          # For the conversion from Psi -> des_quantum

end # BasicSelfConsistent1D


function BasicSelfConsistent1D(positions :: Array{Float64,1}, # list of sites positions in nm
                               mass,                          # A function of position adimensional
                                                              # = 0.0 outside of the quantum region
                               band_offset,                   # A function of position in eV
                               dielectric,                    # A function of positions adimensional
                               dopants,                       # A function of position in 1/nm^3
                               gates :: Tuple{Bool,Bool}  )
    
    
    n_p = length(positions)
    println(" Number of Poisson sites = ",n_p)
    epsilon = map(dielectric,positions)
    dop = map(dopants,positions)
    dop[end] = 0
    dop[1:end-1] .= -Poisson.conversion*dop[1:end-1].*( positions[2:end] .- positions[1:end-1] )
    # The minus sign above comes from the fact that Delta U = - rho/epsilon.
    # Perhaps not the best case to incorporate this minus sign...
    println("Total dopant density = ",sum(dop)/Poisson.conversion, " [1/nm^2]")
    MyP = Poisson.DiscretizedPoissonProblem(dop,epsilon,positions,gates)
    
    where_quantum(x) = (mass(x) > 0. )
    positions_quantum = filter(where_quantum,positions)
    start_quantum = findall(x -> x==positions_quantum[1],positions)[1]
    end_quantum = findall(x -> x==positions_quantum[end],positions)[1]
    println("Quantum region starts at site : ",start_quantum," and ends at site ",end_quantum)
    n_q = length(positions_quantum)
    if n_q != (end_quantum - start_quantum + 1)
        println(n_q," =?= ",end_quantum - start_quantum)
        throw(InvalidStateException)
    end
    println(" Number of quantum sites = ",n_q)
    U_quantum = map(band_offset,positions_quantum)
    mass_quantum = map(mass,positions_quantum) 
    MyQ = Quantum.DiscretizedQuantumProblem(positions[start_quantum:end_quantum],mass_quantum,
                                            U_quantum*Quantum.eV_2_t)
    
    dst_quantum = zeros(n_p)
    U_poisson = zeros(n_p)
    
    mDEG = mass_quantum[1]
    dx = mDEG*Quantum.rho*(positions[start_quantum+1:end_quantum+1]-positions[start_quantum:end_quantum])
    
    BasicSelfConsistent1D(
    positions,
    start_quantum,  
    end_quantum, 
    MyP,
    MyQ,
    dst_quantum,
    U_poisson,
    dx)
end

function resetSolution!(SC ::BasicSelfConsistent1D)
    SC.dst_quantum .= 0.0
    SC.U_poisson .= 0.0
end

function resetQuantumDensity!(SC ::BasicSelfConsistent1D,initial_guess)
    SC.dst_quantum[SC.start_quantum:SC.end_quantum] .= map(initial_guess,SC.positions)[SC.start_quantum:SC.end_quantum] 
end

function ildos(Es,Psis,mu)   # zero temperature version, mu in unit of [t]
    ns = zeros(length(Psis[:,1]))
    for i in 1:length(Es)
        if Es[i] < mu
            ns .+= (mu - Es[i])*Psis[:,i].^2
        end
    end
    ns
end
"""
    mu : in eV
    V_bot, V_top : in V.  Will be ignored with Neumann boundary condition
"""
function solvePoisson!(SC ::BasicSelfConsistent1D,V_bot,V_top)
    #println("Solving Poisson...")
    Poisson.solve_pb(SC.P, SC.U_poisson, SC.dst_quantum*Poisson.conversion;V_bot = V_bot,V_top = V_top)
    #println("...Done")
end

function solveQuantum!(SC ::BasicSelfConsistent1D,mu;N_subbands=4)
    #println("Solving Quantum...")
    Es,Psis = Quantum.solve_pb(SC.Q, -SC.U_poisson[SC.start_quantum:SC.end_quantum]*Quantum.eV_2_t, N_subbands)
    #println("First two subbands bottom = ",Es[1]/Quantum.eV_2_t," eV and ",Es[2]/Quantum.eV_2_t," eV.")
    SC.dst_quantum[SC.start_quantum:SC.end_quantum] .= SC.dx .* ildos(Es/Quantum.eV_2_t,Psis,mu)
    #println("Total 2DEG density = ",sum(SC.dst_quantum)," in [1/nm^2]")
    #println("...Done")
end

function bareIteration!(SC ::BasicSelfConsistent1D,mu,V_bot,V_top;N_subbands=4)
    println("*** New bare Iteration ***")
    solvePoisson!(SC,V_bot,V_top)
    solveQuantum!(SC,mu;N_subbands=N_subbands)
end

function simpleMixing!(SC ::BasicSelfConsistent1D,mu,V_bot,V_top;N_subbands=4,lambda_max = 0.1)
    #println("\n\n*** New simple Mixing Iteration ***\n")
    solvePoisson!(SC,V_bot,V_top)
    n_old = sum(SC.dst_quantum)
    dst_old = copy(SC.dst_quantum)
    solveQuantum!(SC,mu;N_subbands=N_subbands)
    n_new = sum(SC.dst_quantum)
    lambda = lambda_max
    coef = 1.
    if n_old != 0 
        coef = n_new/n_old
    end
    lambda = coef > 1. ? lambda/coef : lambda
    #println(" coef = ",coef," *** lambda = ",lambda)
    SC.dst_quantum .= (1-lambda) * dst_old .+ lambda * SC.dst_quantum    
end

end # QuantumPoisson



    