# fsc1D_julia

Basic 1D self-consistent quantum electrostatic solver



*  Poisson.jl:
     -  Basic Poisson solver
     -  charges are per cell  [V/nm^2]
     -  voltage in [V]
     -  when a gate is present the output voltage at the gate is the charge.

*  Quantum.jl:
     -  Basic Quantum solver

*  QuantumPoisson.jl:
     -  Deal with the self consistency
     -  Properly change the units
    
