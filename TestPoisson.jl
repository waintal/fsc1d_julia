
using Plots
include("Poisson.jl")

function TestPoisson1(q_d,gates,V_bot,V_top)
    N = length(q_d)
    eps = ones(N)
    pos = collect(range(0.,N,length = N))
    MyP = Poisson.DiscretizedPoissonProblem(q_d,eps,pos,gates )
    U = zeros(N)
    q = zeros(N)
    Poisson.solve_pb(MyP, U, q;V_bot = V_bot,V_top = V_top)
    U
end

function PlotTestPoisson1(N)
    pos = collect(range(0.,N,length = N))
    q_d = zeros(N)
    gates = (true,true)
    U = TestPoisson1(q_d,gates,0,1)
    gr()
    p = plot(pos[2:end-1],
             U[2:end-1],
             seriestype=:scatter,
             xlabel ="z [nm]",
             ylabel = "U(z)  [Volts]",
             title = "See Test.jl  PlotTestPoisson1(N)",
             label = "Poisson solver",
             framestyle= :box,
             grid = :off)
    plot!(p,pos,pos/N,label = "Exact solution")
    savefig("TestOutputs/PlotTestFunction1.pdf")
    display(p)
end

function PlotTestPoisson2()
    N=5000
    q_d = zeros(5*N)
    q_d[2*N:3*N-1] .= 2/N
    q_d[3*N:4*N-1] .= -2/N
    eps = ones(5*N)
    pos = collect(range(0.,10,length = 5*N))
    gates = (true,false)
    MyP = Poisson.DiscretizedPoissonProblem(q_d,eps,pos,gates )
    U = zeros(5*N)
    q = zeros(5*N)
    Poisson.solve_pb(MyP, U, q;V_bot = 0,V_top = 0)
    gr()
    p = plot(pos[2:end-1],
             U[2:end-1],
             line = :dot,
             lw = 3,
             xlabel ="z [nm]",
             ylabel = "\$U(z)\$",
             title = "See Test.jl  PlotTestPoisson2()",
             label = "Poisson solver",
             framestyle= :box,
             grid = :on)
    function Exact(z)
        if z<4
            return 0
        elseif z < 6
            return (z-4)^2/2
        elseif z < 8
            return (6-4)^2/2 + 2*(z-6) - (z-6)^2/2
        else
            return (6-4)^2/2 + 2*(8-6) - (8-6)^2/2 
        end
    end     
    plot!(p,pos,Exact.(pos),label = "Exact solution")
    savefig("TestOutputs/PlotTestFunction2.pdf")
end

#PlotTestPoisson1(10)
PlotTestPoisson2()
