using Plots
include("Quantum.jl")


"""
    This test corresponds to a 5.6 nm Al_0.3 Ga_0.7 As - GaAs - AlGaAs quantum well as
    described in the Tan et al J. Appl. Phys. 68, 4073 (1990) paper
"""
function TestQuantum1()
    P = 10000
    N = 4*P
    pos = zeros(N)
    for i in 1:P
        pos[i]=-2.8 - 1/P +50*(i-P)/P
        pos[i+P] = -2.8  + 1/P + (2.8 -2/P)*(i-1)/(P-1)
        pos[i+2*P]= 2.8*(i-1)/P
        pos[i+3*P]= 2.8 + 1/P + 50*(i-1)/P
    end
    #pos = collect(range(-36,36,length = N))
    U = zeros(N)
    mass = ones(N)
    mass .*= 0.067
   
    for i in 1:N
        if pos[i] > 2.8 || pos[i] < -2.8
            U[i] = 0.23*Quantum.eV_2_t
            mass[i] = 0.092
        end
    end
    
    MyQ = Quantum.DiscretizedQuantumProblem(pos,mass,U)
            
    U2 = zeros(N)
    Es,Psis = Quantum.solve_pb(MyQ, U2, 3)

    println("First  Energy : ",1000*Es[1]/Quantum.eV_2_t,"   Expected : 64.24722 meV")
    println("Second Energy : ",1000*Es[2]/Quantum.eV_2_t,"   Expected : 220.7776 meV")
    gr()
    p = plot(pos,
             Psis[:,1:2],
             #line = :dot,
             #lw = 3,
             xlabel ="z [nm]",
             ylabel = "\$|\\Psi(z)|^2\$",
             title = "See TestQuantum.jl  PlotTestQuantum1()",
             label = "Quantum solver",
             framestyle= :box,
             grid = :on)
    vline!(p,[-2.8,2.8],lw = 0.5)
    #plot(pos)
    savefig("TestOutputs/PlotTestQuantum1.pdf")
end

TestQuantum1()
