using Plots
include("Quantum.jl")


"""
    This test corresponds to a simple harmonic oscillator
"""
function TestQuantum2(N)
    pos = collect(LinRange(-20,20,N))
    U = zeros(N)
    mass = ones(N)
    hbar_omega = 10e-3   # in eV
    println(" Expected energies in meV ",1000*hbar_omega)
    lambda = Quantum.hbar / (hbar_omega*Quantum.m_e*Quantum.ee)^0.5 /Quantum.aa  # in nm  . Extension of a wavepacket
    println(" Typical length = ",lambda)
    coef = (1/4)*hbar_omega^2*Quantum.ee^2 / (Quantum.t^2)
    f(x) = coef*x.^2
    U = f(pos)
  
    MyQ = Quantum.DiscretizedQuantumProblem(pos,mass,U)
            
    U2 = zeros(N)
    Es,Psis = Quantum.solve_pb(MyQ, U2, 3)
    println("First  Energy  [meV] : ",1000*Es[1]*Quantum.t/Quantum.ee)
    println("Second Energy  [meV] : ",1000*Es[2]/Quantum.eV_2_t)
    gr()
    p = plot(pos,U)
    
    dd = pos[2]-pos[1]
    psi_anal_0(x) = 1/(pi^0.25)*exp.(-x.^2/2/lambda^2)/lambda^0.5
    psi0 = psi_anal_0(pos)
    println(" Integral of analytical wavefunction = ",dd*sum(psi0.^2))
    println(" Integral of numerical  wavefunction = ",dd*sum(Psis[:,1].^2))
    p = plot(pos,
             [Psis[:,1],psi0],
             #line = [:dot,],
             lw = 1,
             xlabel ="z [nm]",
             ylabel = "\$\\Psi(z)\$",
             title = "Harmonic oscillator. See TestQuantum2.jl",
             label = ["Ground state Num.", "Same analytical"],
             framestyle= :box,
             grid = :on)
    #vline!(p,[-2.8,2.8],lw = 0.5)
    #plot(pos)
    savefig("TestOutputs/PlotTestQuantum2.pdf")
end

TestQuantum2(50)
