using Plots
include("./QuantumPoisson.jl")
using .QuantumPoisson

"""
    This test corresponds to a simple heterostructure

    nd = 1.92 10^15 m^-2 = 1.92e-3 /nm^2

    Doped GaAs      7.5 nm         ^
    Doped GaAlAs   89.0 nm         |
    GaAlAs         49.3 nm         |
    GaAs          200.0 nm         |
"""
function MakeHeterostructure(N :: Int,V_top)
    d1 = 49.3
    d2 = d1 + 89
    d3 = d2 + 7.5
    n_d = 2e-2/(d3-d1)
    epsilon = 12.5
    m_eff = 0.067
    E_b = 0.232
    println(" Dopant density [1/nm^3] = ",n_d)
    Cd = Poisson.conversion*(d3-d1)^2/2/epsilon
    Cs = Poisson.conversion*d3/epsilon
    println(" Full capa to the 2DEG = ",Cs)
    println(" Capa of the donors to the top gate = ",Cd)
    println(" Expected 2DEG density from minimum model ns = ",(Cd*n_d+V_top)/Cs," [1/nm^2]")
    Cq = 1/(m_eff*Quantum.rho)
    println(" Quantum capa to the 2DEG = ",Cq)
    println(" Expected 2DEG density with quantum capa ns = ",(Cd*n_d+V_top)/(Cs+Cq)," [1/nm^2]")
    
    positions = collect(LinRange(0,d3,N)) #  System starts at - 200 nm ends at 300 nm and has N cells 
    mass(x) = (x > -d1 && x < d1) ? m_eff : 0. #  Must always return Float64. Error if one returns Int
    band_offset(x) = x > 0. ? E_b : 0.
    dielectric(x) = epsilon
    dopants(x) = (x > d1) ? n_d : 0.
    gates = (true, true)
    
    
    SC = QuantumPoisson.BasicSelfConsistent1D(
                               positions,                     # list of sites positions in nm
                               mass,                          # A function of position adimensional
                                                              # = 0 outside of the quantum region
                               band_offset,                   # A function of position in eV
                               dielectric,                    # A function of positions adimensional
                               dopants,                       # A function of position in eV/nm^2
                               gates   )
    return SC
    end # TestQuantumPoisson1

# This first test just solves the minimum model numerically.

V_top = -1
SC = MakeHeterostructure(200,V_top)
QuantumPoisson.resetSolution!(SC)

println("Calling Poisson solver....")
Poisson.solve_pb(SC.P, SC.U_poisson, SC.dst_quantum*Poisson.conversion;V_bot = 0,V_top = V_top)

ns = SC.U_poisson[1]/Poisson.conversion
nsc = SC.U_poisson[end]/Poisson.conversion
println("2DEG density at PESCA level = ",ns,"  [1/nm^2]")
println("nsc = ",nsc," [1/nm^2]")

SC.U_poisson[1]=0
SC.U_poisson[end]= V_top
p_V = plot(SC.positions,SC.U_poisson,label = "iter = 1")

savefig(p_V,"TestOutputs/TestQuantumPoisson_V.pdf")
