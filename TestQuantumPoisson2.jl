using Plots
include("./QuantumPoisson.jl")
using .QuantumPoisson

"""
    This test corresponds to a simple heterostructure

    nd = 1.92 10^15 m^-2 = 1.92e-3 /nm^2

    Doped GaAs      7.5 nm         ^
    Doped GaAlAs   89.0 nm         |
    GaAlAs         49.3 nm         |
    GaAs          200.0 nm         |
"""
function MakeHeterostructure(N :: Int,V_top)
    println("Building the heterostructure...")
    d0 = 50
    d0bis = 20
    d1 = 49.3
    d2 = d1 + 89
    d3 = d2 + 7.5
    n_d = 2e-2/(d3-d1)
    epsilon = 12.5
    m_eff = 0.067
    E_b = 0.232
    println(" Dopant density [1/nm^3] = ",n_d)
    Cd = Poisson.conversion*(d3-d1)^2/2/epsilon
    Cs = Poisson.conversion*d3/epsilon
    println(" Full capa to the 2DEG = ",Cs)
    println(" Capa of the donors to the top gate = ",Cd)
    println(" Expected 2DEG density from minimum model ns = ",(Cd*n_d+V_top)/Cs," [1/nm^2]")
    Cq = 1/(m_eff*Quantum.rho)
    println(" Quantum capa to the 2DEG = ",Cq)
    println(" Expected 2DEG density with quantum capa ns = ",(Cd*n_d+V_top)/(Cs+Cq)," [1/nm^2]")
    
    positions = collect(LinRange(-d0,d3,N))    #  System starts at - d0 nm ends at d3 nm and has N cells 
    mass(x) = (x > -d0 && x < d0bis) ? m_eff : 0. #  Must always return Float64. Error if one returns Int
                                               #  0. outside of the quantum region.
    band_offset(x) = x > 0. ? E_b : 0.
    dielectric(x) = epsilon
    dopants(x) = (x > d1) ? n_d : 0.
    gates = (false, true)
    
    
    SC = QuantumPoisson.BasicSelfConsistent1D(
                               positions,                     # list of sites positions in nm
                               mass,                          # A function of position adimensional
                                                              # = 0 outside of the quantum region
                               band_offset,                   # A function of position in eV
                               dielectric,                    # A function of positions adimensional
                               dopants,                       # A function of position in eV/nm^2
                               gates   )
    println("...done")
    return SC
    end # TestQuantumPoisson1

# This first test just solves the minimum model numerically.
gr()
V_top = -1
mu = 0
V_bot = 0
N = 300
N_iter = 1000
SC = MakeHeterostructure(N,V_top)
QuantumPoisson.resetSolution!(SC)

# First run with zero initial density
initial_guess(x) =  0.
QuantumPoisson.resetQuantumDensity!(SC,initial_guess)
p_dst = plot(SC.positions,SC.dst_quantum,label = "iter = 0")
p_V = plot(SC.positions,SC.U_poisson,label = "iter = 0")

n_converg = [sum(SC.dst_quantum)]

for i in 1:N_iter
    QuantumPoisson.simpleMixing!(SC,mu,V_bot,V_top;N_subbands=4,lambda_max = 0.01)
    println("Iter ",i," ** dst_2D = ",sum(SC.dst_quantum)," in [1/nm^2]")
    push!(n_converg, sum(SC.dst_quantum))
end
plot!(p_V,SC.positions,SC.U_poisson,label = string("iter = ",string(N_iter)) )
plot!(p_dst,SC.positions,SC.dst_quantum,label = string("iter = ",string(N_iter)) )

# Second run with a square initial density (try reducing the number of iterations)
initial_guess(x) = (x<0 && x>-10) ? 0.0018/10 : 0.
QuantumPoisson.resetQuantumDensity!(SC,initial_guess)
plot!(p_V,SC.positions,SC.U_poisson,line = :dot,lw=2,label = string("square iter = ",string(0)) )
plot!(p_dst,SC.positions,SC.dst_quantum,line = :dot,lw=2,label = string("square iter = ",string(0)) )

for i in 1:N_iter
    QuantumPoisson.simpleMixing!(SC,mu,V_bot,V_top;N_subbands=4,lambda_max = 0.01)
    println("Iter ",i," ** dst_2D = ",sum(SC.dst_quantum)," in [1/nm^2]")
    push!(n_converg, sum(SC.dst_quantum))
end
plot!(p_V,SC.positions,SC.U_poisson,line = :dot,lw=2,label = string("square iter = ",string(N_iter)) )
plot!(p_dst,SC.positions,SC.dst_quantum,line = :dot,lw=2,label = string("square iter = ",string(N_iter)) )

# Plot of the total energy seen by the electrons.
band_offset(x) = (x > 0. && x < 138.3) ? 0.232 : 0.
TotalE = map(band_offset,SC.positions)
# remember that SC.U_poisson[end] holds the value of the charge underneath the gate.
SC.U_poisson[end] = V_top
TotalE .= TotalE - SC.U_poisson
p_total = plot(SC.positions,TotalE,label = "Total energy versus position")

p_conv = plot(n_converg,label = "convergence of total density")

savefig(p_V,"TestOutputs/TestQuantumPoisson2_V.pdf")
savefig(p_dst,"TestOutputs/TestQuantumPoisson2_dst.pdf")
savefig(p_conv,"TestOutputs/TestQuantumPoisson2_conv.pdf")
savefig(p_total,"TestOutputs/TestQuantumPoisson2_tot.pdf")



