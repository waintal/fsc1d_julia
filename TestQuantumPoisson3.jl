using Plots
include("./QuantumPoisson.jl")
using .QuantumPoisson

"""
    This test corresponds to a simple heterostructure

    nd = 1.92 10^15 m^-2 = 1.92e-3 /nm^2

    Doped GaAs      7.5 nm         ^
    Doped GaAlAs   89.0 nm         |
    GaAlAs         49.3 nm         |
    GaAs          200.0 nm         |
"""
function MakeHeterostructure(N :: Int,V_top)
    println("Building the heterostructure...")
    d0 = 50
    d0bis = 20
    d1 = 49.3
    d2 = d1 + 89
    d3 = d2 + 7.5
    n_d = 2e-2/(d3-d1)
    epsilon = 12.5
    m_eff = 0.067
    E_b = 0.232
    println(" Dopant density [1/nm^3] = ",n_d)
    Cd = Poisson.conversion*(d3-d1)^2/2/epsilon
    Cs = Poisson.conversion*d3/epsilon
    println(" Full capa to the 2DEG = ",Cs)
    println(" Capa of the donors to the top gate = ",Cd)
    println(" Expected 2DEG density from minimum model ns = ",(Cd*n_d+V_top)/Cs," [1/nm^2]")
    Cq = 1/(m_eff*Quantum.rho)
    println(" Quantum capa to the 2DEG = ",Cq)
    println(" Expected 2DEG density with quantum capa ns = ",(Cd*n_d+V_top)/(Cs+Cq)," [1/nm^2]")
    
    positions = collect(LinRange(-d0,d3,N))    #  System starts at - d0 nm ends at d3 nm and has N cells 
    mass(x) = (x > -d0 && x < d0bis) ? m_eff : 0. #  Must always return Float64. Error if one returns Int
                                               #  0. outside of the quantum region.
    band_offset(x) = x > 0. ? E_b : 0.
    dielectric(x) = epsilon
    dopants(x) = (x > d1) ? n_d : 0.
    gates = (false, true)
    
    
    SC = QuantumPoisson.BasicSelfConsistent1D(
                               positions,                     # list of sites positions in nm
                               mass,                          # A function of position adimensional
                                                              # = 0 outside of the quantum region
                               band_offset,                   # A function of position in eV
                               dielectric,                    # A function of positions adimensional
                               dopants,                       # A function of position in eV/nm^2
                               gates   )
    println("...done")
    return SC
    end # TestQuantumPoisson1

# This first test just solves the minimum model numerically.
gr()
V_top = -1
mu = 0
V_bot = 0
N = 300
N_iter = 1000
SC = MakeHeterostructure(N,V_top)

Vs = LinRange(-1.5,-0.5,50)
dst = []
barycenter = []
initial_guess(x) =  0.

for V in Vs
    QuantumPoisson.resetQuantumDensity!(SC,initial_guess)
    for i in 1:N_iter
        QuantumPoisson.simpleMixing!(SC,mu,V_bot,V;N_subbands=4,lambda_max = 0.01)
    end
    push!(dst, sum(SC.dst_quantum))
    push!(barycenter, sum(SC.dst_quantum .* SC.positions)/sum(SC.dst_quantum))    
end

p_dst = plot(Vs,dst,label = "density versus V_sc")
p_V = plot(Vs,barycenter,label = "Gas position versus V_sc")

savefig(p_V,"TestOutputs/TestQuantumPoisson2_pos_vs_Vsc.pdf")
savefig(p_dst,"TestOutputs/TestQuantumPoisson2_dst_vs_Vsc.pdf")


